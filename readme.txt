Python code to visualize the relation between two variables in the model output 'dpp0029'.

2D and 3D output variables could be compared. The variables to compare are selected when initialising
an instance of 'PropertiesToCompare' (search the code for 'self.varX ='). Data file names for
corresponding variables are specified in the very beginning of the code. The code is tested with the
following data:
'dpp0029_cl_1x1_all_pl.nc'
'dpp0029_prw_1x1_all.nc'
'dpp0029_cllvi_1x1_all.nc'
'dpp0029_clivi_1x1_all.nc'

The data files are expected to be in the subfolder “data” located in the folder with the code.

To run the code few extra libraries are needed, like “bokeh” (tested with the version 2.3.0) and geopandas.

This code relies on python interpreter when providing the reaction on changing the control slider
states, therefore the code does not build a stand alone html but produces an output plotted in
default browser and served by bokeh.

Five plots are created and shown in separate tabs. The first two tabs show the selected variable
values sorted as histogram. Third tab represents the regression, in which 'varY' values paired
with 'varX' are summed for each histogram cell of 'varX'. Fourth tab plots the pairs varX:varY.
Last tab shows the red box indicating the area of analysis on the geo map. Small copy of geo map
is also shown in the 'controls' tab.  

The period of analysis, the number of days to average over, the pressure range (for 3D variable),
as well as the latitude and longitude could be selected with the sliders in the 'controls' tab.
The size of area taken for analysis is hardcoded and set to 3x3 deg.

Note: time indexes mismatching between 2D and 3D variables are dropped (hardcoded).

To run the code one can start it from a terminal like “bokeh serve —show compare_v3.1.py”.
As another option, one can run it in Spyder console with two steps:
import os
os.system("bokeh serve --show compare_v3.1.py")