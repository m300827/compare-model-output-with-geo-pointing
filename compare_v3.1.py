#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 14 14:04:10 2021

@author: ilya
"""
from datetime import datetime
import numpy as np
import xarray as xr

from bokeh.plotting import figure, curdoc
from bokeh.models import Panel, Tabs, ColumnDataSource, Slider, RangeSlider
from bokeh.models.widgets import DateRangeSlider
from bokeh.layouts import row, column

import geopandas
from bokeh.models import GeoJSONDataSource

bin_number = 20

file_path = 'data/'
# file_path = '/Users/ilya/Documents/code/dyamond/data/'
file_name_cl = 'dpp0029_cl_1x1_all_pl.nc'
file_name_iwv = 'dpp0029_prw_1x1_all.nc'
file_name_lwp = 'dpp0029_cllvi_1x1_all.nc'
file_name_iwp = 'dpp0029_clivi_1x1_all.nc'

time_window = 21

lat1 = 13.
lon1 = -59.

lat2 = lat1 + 3.
lon2 = lon1 + 3.

pressure_low = 69000.   #[Pa]
pressure_high = 100000. #[Pa]

# plot dimentions
gap = 10
gaps = [gap, gap, gap, gap]
mini_map_width = 270
mini_map_height = 180

world = geopandas.read_file(geopandas.datasets.get_path('naturalearth_lowres'))
continents = world[['continent', 'geometry']].dissolve(by='continent')
geosource = GeoJSONDataSource(geojson=continents.to_json())
probe_cell_source = ColumnDataSource(data=dict(x=[lon1,lon2,lon2,lon1,lon1], y=[lat1,lat1,lat2,lat2,lat1]))

plot_glob_map = figure(tools="box_zoom,zoom_in,zoom_out,undo,redo,reset,save", min_border=10,
              x_range=[-180.,180.], y_range=[-90.,90.],
              toolbar_location="above", y_axis_location="left", sizing_mode='stretch_both',toolbar_sticky=True)
plot_glob_map.toolbar.active_drag = None
plot_glob_map.axis.axis_label_text_font_style="normal"
plot_glob_map.axis.axis_label_text_font_size = '20px'
plot_glob_map.axis.major_label_text_font_size = '17px'
plot_glob_map.toolbar.active_inspect = []#[hover_tool, crosshair_tool]
plot_glob_map.patches('xs', 'ys', source=geosource, fill_color=None, line_color='black',
              line_width=1., fill_alpha=1.)
plot_glob_map.patch('x', 'y', source=probe_cell_source, fill_color='red', line_color='black',
              line_width=1., fill_alpha=1.)

plot_glob_mini_map = figure(tools="", min_border=10, margin=gaps, title='Location',
              x_range=[-180.,180.], y_range=[-90.,90.], plot_width=mini_map_width, plot_height=mini_map_height,
              toolbar_location="above", y_axis_location="left", sizing_mode='fixed',toolbar_sticky=True)
plot_glob_mini_map.axis.visible = False
plot_glob_mini_map.patches('xs', 'ys', source=geosource, fill_color=None, line_color='black',
              line_width=1., fill_alpha=1.)
plot_glob_mini_map.patch('x', 'y', source=probe_cell_source, fill_color='red', line_color='red',
              line_width=1., fill_alpha=1.)

class Variable_2D:
    def __init__(self, file_path, file_name, var_ncname, var_label, var_long_name=None):
        self.data_source = ColumnDataSource(data=dict(x=[0.,1.], y=[0.,1.], w=[0.,1.]))
        self.read(file_path, file_name, var_ncname, var_label, var_long_name)
        self.process()
        self.fill_datasource()
        self.plotting_histogram()

    def read(self, file_path, file_name, var_ncname, var_label, var_long_name):
        self.times = xr.open_dataset(file_path+file_name)['time']
        self.data = xr.open_dataset(file_path+file_name)[var_ncname]
        self.name = var_label #self.data.attrs['standard_name']
        if var_long_name==None:
            self.long_name = self.data.attrs['long_name']
        else:
            self.long_name = var_long_name
        self.units = self.data.attrs['units']

    def process(self):

        data_tmp = self.data.sel(time=slice(days_from,days_to), lon=slice(lon1,lon2), lat=slice(lat1,lat2)).load()
        time_to_drop = np.datetime64('2021-01-25T09:00:00.000000000')
        data_tmp = data_tmp.where(data_tmp.time!=time_to_drop, drop=True)
        self.val = data_tmp.rolling(time=time_window).mean().values.flatten()

    def fill_datasource(self):
        self.range_from = np.nanmin(self.val)
        self.range_to = np.nanmax(self.val)
        self.hist, bin_edges = np.histogram(self.val, bins=bin_number, range=(self.range_from, self.range_to))
        self.bin_centers = (bin_edges[0:bin_number] + bin_edges[1:bin_number+1]) / 2.
        self.bin_width = bin_edges[1] - bin_edges[0]
        self.data_source.data=dict(x=self.bin_centers, y=self.hist,
                                   w=self.bin_width*np.ones_like(self.hist, dtype=float))

    def update(self):
        self.process()
        self.fill_datasource()

    def plotting_histogram(self):
        self.plot_hist = figure(tools="box_zoom,zoom_in,zoom_out,undo,redo,reset,save", min_border=10,
                        toolbar_location="above", x_axis_label=self.long_name + ', ' + self.units,
                        y_axis_label='Case number', #x_range=[self.range_from, self.range_to],
                        y_axis_location="left", sizing_mode='stretch_both',toolbar_sticky=True)
        self.plot_hist.toolbar.active_drag = None
        self.plot_hist.axis.axis_label_text_font_style="normal"
        self.plot_hist.axis.axis_label_text_font_size = '20px'
        self.plot_hist.axis.major_label_text_font_size = '17px'
        self.plot_hist.toolbar.active_inspect = []#[hover_tool, crosshair_tool]
        self.plot_hist.vbar(x='x', bottom=0, top='y', width='w', source=self.data_source, fill_color="#b3de69")

class Cl(Variable_2D):
    def __init__(self, file_path, file_name, var_ncname, var_label, var_long_name=None):
        super().__init__(file_path, file_name, var_ncname, var_label, var_long_name)
    def process(self):

        data_tmp = self.data.sel(time=slice(days_from,days_to), lon=slice(lon1,lon2),
                                 lat=slice(lat1,lat2), plev=slice(pressure_low, pressure_high)).load()
        time_to_drop = np.datetime64('2020-01-21T09:00:00.000000000')
        data_tmp = data_tmp.where(data_tmp.time!=time_to_drop, drop=True)
        clx = data_tmp.rolling(time=time_window).mean().values
        self.val = np.nansum(clx, axis=1).flatten()

class PropertiesToCompare:
    def __init__(self):
        global days_from, days_to
        times = xr.open_dataset(file_path+file_name_cl)['time'].values
        # set outer boundes of datetime period
        days_from = str(np.datetime_as_string(times[0], unit='D'))
        days_to = str(np.datetime_as_string(times[-1], unit='D'))
        
        self.data_source = ColumnDataSource(data=dict(x=[0.,1.], y=[0.,1.]))
        self.data_source_regression = ColumnDataSource(data=dict(x=[0.,1.], y=[0.,1.], w=[0.,1.]))

        # water liquid, ice or vapor versus cloud area fraction
        self.varX = Cl(file_path, file_name_cl, 'cl', 'CF')                 # cloud area fraction
        # self.varY = Variable_2D(file_path, file_name_lwp, 'cllvi', 'LWP') # liquid
        self.varY = Variable_2D(file_path, file_name_iwp, 'clivi', 'IWP')   # ice
        # self.varY = Variable_2D(file_path, file_name_iwv, 'prw', 'IWV')   # vapor

        self.browserTitle = self.varY.name + '-' + self.varX.name

        self.process()
        self.plotting_pairs()
        self.plotting_regression()
        self.time_window = time_window
        self.lat1 = lat1
        self.lat2 = lat2
        self.lon1 = lon1
        self.lon2 = lon2
        self.pressure_low = pressure_low
        self.pressure_high = pressure_high

        self.slider_days = DateRangeSlider(start=days_from, end=days_to, value=(days_from, days_to),
                                   step=1, margin=gaps, title='Period', width=mini_map_width)
        self.slider_days.on_change('value_throttled', self.update_days)

        self.slider_time_window = Slider(start=1, end=30, value=self.time_window, margin=gaps,
                                         title='Averaging interval, days', width=mini_map_width)
        self.slider_time_window.on_change('value_throttled', self.updatetime_window)

        self.slider_lat = Slider(start=-90, end=87, value=self.lat1, margin=gaps,
                                 title='Latitude', width=mini_map_width)
        self.slider_lat.on_change('value_throttled', self.update_lat)
        self.slider_lat.on_change('value', self.update_map_lat)

        self.slider_lon = Slider(start=-180., end=177., value=self.lon1, margin=gaps,
                                 title='Longitude', width=mini_map_width)
        self.slider_lon.on_change('value_throttled', self.update_lon)
        self.slider_lon.on_change('value', self.update_map_lon)

        self.slider_pressure = RangeSlider(start=0., end=1000., margin=gaps,
                                   value=(self.pressure_low/100., self.pressure_high/100.),
                                   step=50., title='Pressure levels, mbar', width=mini_map_width)
        self.slider_pressure.on_change('value_throttled', self.update_pressure)

        controls = column(self.slider_days, self.slider_time_window, self.slider_pressure,
                          plot_glob_mini_map, self.slider_lat, self.slider_lon)

        self.panel_controls = Panel(child=controls, title='controls')

    def process(self):
        ind = np.argsort(self.varX.val)
        self.varX_sorted = self.varX.val[ind]
        self.varY_sorted = self.varY.val[ind]
        self.data_source.data=dict(x=self.varX_sorted, y=self.varY_sorted)

        varY_mean = np.zeros_like(self.varX.hist, dtype=float)
        j0 = 0
        for i in range(bin_number):
                j1 = j0 + self.varX.hist[i]
                varY_mean[i] = np.nanmean(self.varY_sorted[j0:j1])
                j0 = j1
        self.data_source_regression.data=dict(x=self.varX.bin_centers, y=varY_mean,
                                              w=self.varX.bin_width*np.ones_like(varY_mean, dtype=float))
        
    def plotting_pairs(self):
        self.plot_pair = figure(tools="box_zoom,zoom_in,zoom_out,undo,redo,reset,save", min_border=10,
                  toolbar_location="above", x_axis_label=self.varX.long_name + ', ' + self.varX.units,
                  y_axis_label=self.varY.long_name + ', ' + self.varY.units, #title=title, 
                  y_axis_location="left", sizing_mode='stretch_both',toolbar_sticky=True)
        self.plot_pair.toolbar.active_drag = None
        self.plot_pair.axis.axis_label_text_font_style="normal"
        self.plot_pair.axis.axis_label_text_font_size = '20px'
        self.plot_pair.axis.major_label_text_font_size = '17px'
        self.plot_pair.toolbar.active_inspect = []#[hover_tool, crosshair_tool]
        self.plot_pair.circle(x='x', y='y', source=self.data_source, size=10, fill_color="#b3de69")

    def plotting_regression(self):
        self.plot_regression = figure(tools="box_zoom,zoom_in,zoom_out,undo,redo,reset,save", min_border=10,
                          toolbar_location="above", x_axis_label=self.varX.long_name + ', ' + self.varX.units,
                          y_axis_label=self.varY.long_name + ', ' + self.varY.units, #title=title, 
                          y_axis_location="left", sizing_mode='stretch_both',toolbar_sticky=True)
        self.plot_regression.toolbar.active_drag = None
        self.plot_regression.axis.axis_label_text_font_style="normal"
        self.plot_regression.axis.axis_label_text_font_size = '20px'
        self.plot_regression.axis.major_label_text_font_size = '17px'
        self.plot_regression.toolbar.active_inspect = []#[hover_tool, crosshair_tool]
        self.plot_regression.vbar(x='x', bottom=0., top='y', source=self.data_source_regression,
                              width='w', fill_color="#b3de69")

    def update_days(self, attr, old, new):
        global days_from, days_to
        days_from = datetime.utcfromtimestamp(new[0]/1000.).strftime('%Y-%m-%d')
        days_to = datetime.utcfromtimestamp(new[1]/1000.).strftime('%Y-%m-%d')
        self.varX.update()
        self.varY.update()
        print('')
        print(self.varX.val.shape)
        print(self.varY.val.shape)
        print('')
        self.process()

    def updatetime_window(self, attr, old, new):
        global time_window
        self.time_window = new
        time_window = new
        self.varX.update()
        self.varY.update()
        self.process()

    def update_map_lat(self, attr, old, new):
        global lat1, lat2
        self.lat1 = new
        lat1 = new
        self.lat2 = self.lat1 + 3.
        lat2 = lat1 + 3.
        probe_cell_source.data=dict(x=[lon1,lon2,lon2,lon1,lon1], y=[lat1,lat1,lat2,lat2,lat1])

    def update_lat(self, attr, old, new):
        global lat1, lat2
        self.lat1 = new
        lat1 = new
        self.lat2 = self.lat1 + 3.
        lat2 = lat1 + 3.
        self.varX.update()
        self.varY.update()
        self.process()
        probe_cell_source.data=dict(x=[lon1,lon2,lon2,lon1,lon1], y=[lat1,lat1,lat2,lat2,lat1])

    def update_map_lon(self, attr, old, new):
        global lon1, lon2
        self.lon1 = new
        lon1 = new
        self.lon2 = self.lon1 + 3.
        lon2 = lon1 + 3.
        probe_cell_source.data=dict(x=[lon1,lon2,lon2,lon1,lon1], y=[lat1,lat1,lat2,lat2,lat1])

    def update_lon(self, attr, old, new):
        global lon1, lon2
        self.lon1 = new
        lon1 = new
        self.lon2 = self.lon1 + 3.
        lon2 = lon1 + 3.
        self.varX.update()
        self.varY.update()
        self.process()
        probe_cell_source.data=dict(x=[lon1,lon2,lon2,lon1,lon1], y=[lat1,lat1,lat2,lat2,lat1])

    def update_pressure(self, attr, old, new):
        global pressure_low, pressure_high
        self.pressure_low = new[0] * 100.
        pressure_low = new[0] * 100.
        self.pressure_high = new[1] * 100.
        pressure_high = new[1] * 100.
        self.varX.update()
        self.varY.update()
        self.process()

dataSet = PropertiesToCompare()

panel_a = Panel(child=dataSet.varX.plot_hist, title=dataSet.varX.name)
panel_b = Panel(child=dataSet.varY.plot_hist, title=dataSet.varY.name)
panel_c = Panel(child=dataSet.plot_regression, title='Regression')
panel_d = Panel(child=dataSet.plot_pair, title='Pairs')
panel_e = Panel(child=plot_glob_map, title='Location')

figure_tab = Tabs(tabs=[ panel_a, panel_b, panel_c, panel_d, panel_e ], active=3)
control_tab = Tabs(tabs=[ dataSet.panel_controls ])

layout = row(figure_tab, control_tab, sizing_mode='stretch_height')
curdoc().add_root(layout)
curdoc().title = dataSet.browserTitle



